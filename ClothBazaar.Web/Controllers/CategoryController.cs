﻿using ClothBazar.Services;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClothBazar.Database;

namespace ClothBazaar.Web.Controllers
{
          public class CategoryController : Controller
          {
                    CategoriesServices categoryServices = new CategoriesServices();
                    // GET: Category
                    [HttpGet]
                    public ActionResult Index()
                    {
                              CBContext obj = new CBContext();
                              var get = obj.Categories.ToList();
                              return View(get);
                              //var categories = categoryServices.GetCategories();
                              //return View(categories);
                    }

                    [HttpGet]
                    public ActionResult Category()
                    {
                              return View();
                    }

                    [HttpPost]
                    public ActionResult Category(Category category)
                    {
                              categoryServices.SaveCategory(category);

                              return RedirectToAction("Index");
                    }

                    [HttpGet]
                    public ActionResult Edit(int ID)
                    {
                              var category = categoryServices.GetCategory(ID);
                              return View(category);
                    }

                    [HttpPost]
                    public ActionResult Edit(Category category)
                    {
                              categoryServices.UpdateCategory(category);
                              return RedirectToAction("Index");
                    }

                    [HttpGet]
                    public ActionResult Delete(int ID)
                    {
                              var category = categoryServices.GetCategory(ID);
                              return View(category);
                    }

                    [HttpPost]
                    public ActionResult Delete(Category category)
                    {
                              //category = categoryServices.GetCategory(category.ID);
                              categoryServices.DeleteCategory(category.ID);
                              return RedirectToAction("Index");
                    }
          }
}