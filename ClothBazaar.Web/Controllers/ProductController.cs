﻿using ClothBazar.Services;
using ClothBazar.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClothBazar.Database;

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using ClothBazar.Services;

namespace ClothBazaar.Web.Controllers
{
          public class ProductController : Controller
          {
                    ProductsServices products = new ProductsServices();
                    // GET: Product
                    public ActionResult Index()
                    {
                              return View();
                    }

                    public ActionResult ProductTable(string search)
                    {
                            var product = products.GetProducts().ToList();
                              if (string.IsNullOrEmpty(search) == false)
                              {
                                        product = product.Where(p => p.Name != null && p.Name.ToLower().Contains(search.ToLower())).ToList();
                              }
                              return PartialView(product);
                    }
                    
                    [HttpGet]
                    public ActionResult Create()
                    {
                              return View();
                    }

                    [HttpPost]
                    public ActionResult Create(Product product)
                    {
                              products.SaveProduct(product);
                              return RedirectToAction("ProductTable");
                    }

                    //[HttpGet]
                    //public ActionResult DeleteP(int ID)
                    //{
                    //          var productID = products.GetProduct(ID);
                    //          return View(productID);
                    //}

                    [HttpPost]
                    public ActionResult Delete(int ID)
                    {
                              //category = categoryServices.GetCategory(category.ID);
                              products.DeleteProduct(ID);
                              return RedirectToAction("ProductTable");
                    }

                    [HttpGet]
                    public ActionResult Edit(int Id)
                    {
                              var fetch = products.GetProduct(Id);
                              return View(fetch);
                    }

                    [HttpPost]
                    public ActionResult Edit(Product product)
                    {
                              products.UpdateProduct(product);
                              return RedirectToAction("ProductTable");
                    }
          }
}